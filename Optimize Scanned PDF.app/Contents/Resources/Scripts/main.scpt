FasdUAS 1.101.10   ��   ��    k             l     ��  ��      OCR This (Acrobat)     � 	 	 &   O C R   T h i s   ( A c r o b a t )   
  
 l     ��  ��    1 + Original Script by Joe Kissell <jk@alt.cc>     �   V   O r i g i n a l   S c r i p t   b y   J o e   K i s s e l l   < j k @ a l t . c c >      l     ��  ��    4 . Modified by David Klem <david.klem@gmail.com>     �   \   M o d i f i e d   b y   D a v i d   K l e m   < d a v i d . k l e m @ g m a i l . c o m >      l     ��  ��    R L "How to make your office paperless" http://www.macworld.com/article/159007/     �   �   " H o w   t o   m a k e   y o u r   o f f i c e   p a p e r l e s s "   h t t p : / / w w w . m a c w o r l d . c o m / a r t i c l e / 1 5 9 0 0 7 /      l     ��  ��    _ Y "Take Control of Your Paperless Office" http://www.takecontrolbooks.com/paperless-office     �   �   " T a k e   C o n t r o l   o f   Y o u r   P a p e r l e s s   O f f i c e "   h t t p : / / w w w . t a k e c o n t r o l b o o k s . c o m / p a p e r l e s s - o f f i c e      l     ��������  ��  ��       !   l     �� " #��   " H B on adding folder items to this_folder after receiving these_items    # � $ $ �   o n   a d d i n g   f o l d e r   i t e m s   t o   t h i s _ f o l d e r   a f t e r   r e c e i v i n g   t h e s e _ i t e m s !  % & % i      ' ( ' I     �� )��
�� .aevtodocnull  �    alis ) o      ���� 0 these_items  ��   ( k     � * *  + , + O     - . - r     / 0 / n     1 2 1 1   	 ��
�� 
pnam 2 5    	�� 3��
�� 
appf 3 m     4 4 � 5 5 * c o m . a d o b e . A c r o b a t . P r o
�� kfrmID   0 o      ���� 0 appname appName . m      6 6�                                                                                  MACS  alis    t  Macintosh HD               Ʉ֊H+   R�t
Finder.app                                                      SH��(tw        ����  	                CoreServices    Ʌ�      �(��     R�t R�g R�f  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��   ,  7 8 7 O    9 : 9 r     ; < ; n     = > = 1    ��
�� 
vers > 5    �� ?��
�� 
appf ? m     @ @ � A A * c o m . a d o b e . A c r o b a t . P r o
�� kfrmID   < o      ���� 0 
appversion 
appVersion : m     B B�                                                                                  MACS  alis    t  Macintosh HD               Ʉ֊H+   R�t
Finder.app                                                      SH��(tw        ����  	                CoreServices    Ʌ�      �(��     R�t R�g R�f  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��   8  C�� C Y    � D�� E F�� D k   - � G G  H I H r   - 3 J K J n   - 1 L M L 4   . 1�� N
�� 
cobj N o   / 0���� 0 i   M o   - .���� 0 these_items   K o      ���� 0 	this_item   I  O P O r   4 ; Q R Q I  4 9�� S��
�� .sysonfo4asfe        file S o   4 5���� 0 	this_item  ��   R l      T���� T o      ���� 0 	item_info  ��  ��   P  U V U O   < O W X W k   C N Y Y  Z [ Z I  C H������
�� .miscactvnull��� ��� null��  ��   [  \�� \ I  I N�� ]��
�� .aevtodocnull  �    alis ] o   I J���� 0 	this_item  ��  ��   X 4   < @�� ^
�� 
capp ^ o   > ?���� 0 appname appName V  _ ` _ O   P � a b a O   T � c d c k   [ � e e  f g f I  [ s�� h��
�� .prcsclicuiel    ��� uiel h l  [ o i���� i n   [ o j k j 4   h o�� l
�� 
menI l m   k n m m � n n ( O p t i m i z e   S c a n n e d   P D F k l  [ h o���� o n   [ h p q p 4   a h�� r
�� 
menE r m   d g s s � t t  D o c u m e n t q 4   [ a�� u
�� 
mbar u m   _ `���� ��  ��  ��  ��  ��   g  v w v I  t ��� x��
�� .prcsclicuiel    ��� uiel x n   t � y z y 4   | ��� {
�� 
butT { m    � | | � } }  O K z 4   t |�� ~
�� 
cwin ~ m   x {   � � � ( O p t i m i z e   S c a n n e d   P D F��   w  � � � I  � ��� ���
�� .prcsclicuiel    ��� uiel � l  � � ����� � n   � � � � � 4   � ��� �
�� 
menI � m   � � � � � � �  S a v e � l  � � ����� � n   � � � � � 4   � ��� �
�� 
menE � m   � � � � � � �  F i l e � 4   � ��� �
�� 
mbar � m   � ����� ��  ��  ��  ��  ��   �  � � � I  � ��� ���
�� .prcsclicuiel    ��� uiel � n   � � � � � 4   � ��� �
�� 
butT � m   � � � � � � �  S a v e � 4   � ��� �
�� 
cwin � m   � � � � � � �  S a v e   A s��   �  � � � I  � ��� ���
�� .sysodelanull��� ��� nmbr � m   � � � � ?ٙ�������   �  ��� � I  � ��� ���
�� .prcskprsnull���    utxt � 1   � ���
�� 
spac��  ��   d 4   T X�� �
�� 
pcap � m   V W � � � � �  A c r o b a t b m   P Q � ��                                                                                  sevs  alis    �  Macintosh HD               Ʉ֊H+   R�tSystem Events.app                                               S���         ����  	                CoreServices    Ʌ�      � PB     R�t R�g R�f  =Macintosh HD:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��   `  � � � I  � ��� ���
�� .sysodelanull��� ��� nmbr � m   � ����� ��   �  ��� � O   � � � � � k   � � � �  � � � I  � �������
�� .miscactvnull��� ��� null��  ��   �  ��� � I  � ��� ���
�� .coreclosnull���    obj  � l  � � ����� � 4  � ��� �
�� 
docu � m   � ����� ��  ��  ��  ��   � 4   � ��� �
�� 
capp � o   � ����� 0 appname appName��  �� 0 i   E m   ! "����  F n   " ( � � � m   % '��
�� 
nmbr � n  " % � � � 2  # %��
�� 
cobj � o   " #���� 0 these_items  ��  ��   &  � � � l     ��������  ��  ��   �  � � � l     �� � ���   �   key code 123 -- Left    � � � � *   k e y   c o d e   1 2 3   - -   L e f t �  � � � l     �� � ���   �   key code 124 -- Right    � � � � ,   k e y   c o d e   1 2 4   - -   R i g h t �  � � � l     �� � ���   �   key code 125 -- Down    � � � � *   k e y   c o d e   1 2 5   - -   D o w n �  ��� � l     �� � ���   �   key code 126 -- Up    � � � � &   k e y   c o d e   1 2 6   - -   U p��       �� � ���   � ��
�� .aevtodocnull  �    alis � �� (���� � ���
�� .aevtodocnull  �    alis�� 0 these_items  ��   � �������������� 0 these_items  �� 0 appname appName�� 0 
appversion 
appVersion�� 0 i  �� 0 	this_item  �� 0 	item_info   � $ 6�� 4���� @�������������� �� ��~�} s�| m�{�z �y | � � � � ��x�w�v�u�t
�� 
appf
�� kfrmID  
�� 
pnam
�� 
vers
�� 
cobj
�� 
nmbr
�� .sysonfo4asfe        file
�� 
capp
�� .miscactvnull��� ��� null
�� .aevtodocnull  �    alis
� 
pcap
�~ 
mbar
�} 
menE
�| 
menI
�{ .prcsclicuiel    ��� uiel
�z 
cwin
�y 
butT
�x .sysodelanull��� ��� nmbr
�w 
spac
�v .prcskprsnull���    utxt
�u 
docu
�t .coreclosnull���    obj �� �� *���0�,E�UO� *���0�,E�UO �k��-�,Ekh ��/E�O�j 	E�O*�/ *j O�j UO� s*��/ k*a k/a a /a a /j O*a a /a a /j O*a k/a a /a a /j O*a a /a a /j Oa j O_  j !UUOkj O*�/ *j O*a "k/j #U[OY�Bascr  ��ޭ